# Powerline-shell

This is a powerline clone written in bash made to be easy to add additional displays.

## Installation

1. It is recommended to install a patched font for your shell so it can align the separator correctly. Please see https://github.com/powerline/fonts for more information.
2. Copy the script to any folder (e.g. `~/.powerline.sh`)
3. Edit your `~/.bashrc` and add the following:

    ```
    [[ $- == *i* ]] && . ~/.powerline.sh
    ```

4. Open a new bash prompt to see the new command prompt.


## Configuration

Two files will be created automatically upon bind, which you can adjust and reload your shell.

- $HOME/.powerline-shell.colors: This file contains color mappings that were determined by tput. If you change shells to support more colors, delete this file and it will automatically be regenerated.
- $HOME/.powerline-shell.cfg: Configuration of powerline-shell.

## Acknowledgements

This is based off the work of various powerline projects:

- https://github.com/milkbikis/powerline-shell
- https://github.com/riobard/bash-powerline