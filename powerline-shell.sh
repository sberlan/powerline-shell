#!/usr/bin/env bash

__ps_init() {
    # == Utility functions ===========================
    ps_load_color_file() {
        local color_file="$HOME/.powerline-shell.colors"
        if [ ! -r "$color_file" ]; then
            local cmd
            local colorIndex
            local src
            local mapCmd
            local colors=(BLACK RED GREEN YELLOW BLUE PURPLE CYAN LIGHTGREY DARKGREY BRIGHTRED BRIGHTGREEN BRIGHTYELLOW BRIGHTBLUE BRIGHTPURPLE BRIGHTCYAN WHITE)
            echo "readonly PS_BOLD=\"\[$(tput bold)\]\"" >> "$color_file"
            echo "readonly PS_DIM=\"\[$(tput dim)\]\"" >> "$color_file"
            echo "readonly PS_RESET=\"\[$(tput sgr0)\]\"" >> "$color_file"
            echo "readonly PS_REVERSE=\"\[$(tput rev)\]\"" >> "$color_file"
            for cmd in setab setaf; do
                for colorIndex in {0..15}; do
                    if [ "$cmd" == "setab" ]; then
                        src="BG"
                    else
                        src="FG"
                    fi

                    echo "readonly PS_${src}_${colors[$colorIndex]}=\"\[$(tput $cmd $colorIndex)\]\"" >> "$color_file"
                done
            done
        fi

        . "$color_file"
    }

    ps_load_settings_file() {
        local settings_file=$HOME/.powerline-shell.cfg;
        if [ ! -r "$settings_file" ]; then
            cp "$(dirname "$BASH_SOURCE")/powerline-shell.cfg" "$settings_file"
        fi

        . "$settings_file"
    }

    ps_find_index() {
        local value=$1
        local i=0
        shift

        while [ -n "$1" ]; do
            if [ "$1" == "$value" ]; then
                printf $i;
                return
            fi

            shift
            i=$((i+1))
        done
    }

    ps_add() {
        local bgcolor=$1
        local fgcolor=$2
        local text="${*:3}"
        local newline=0
        local newcols=0
        local index=0

        if [ "$1" == "nl" ]; then
            bgcolor=""
            fgcolor=""
            text="\n"
        fi

        if [ -n "$text" ]; then
            # Truncate the component if it can't possibly fit in the viewport
            if [ ${#text} -gt $PS_COL_WIDTH ]; then
                index=$((${#text} + 3 - $PS_COL_WIDTH))
                if [ $index -gt 0 ]; then
                    text="...${text:$index}"
                fi
            fi

            # If adding it overflows a line, put it on a new line.
            newcols=$(($PS_COLS + 3 + ${#text}))
            local sep_color=$(ps_find_index $PS_BG_LAST ${PS_BG_COLORS[*]})
            if [ $PS_COLS -gt 0 ] && [ $newcols -gt $PS_COL_WIDTH ]; then
                ps_finish 0
                newcols=0
            fi
            PS_COLS=$newcols
            if [ "$PS_NO_COLOR" == "true" ]; then
                # Draw a separator if we need one
                if [ -n "$PS_BG_LAST" ]; then
                    PS1+=" $PS_SHORT_SEPARATOR "
                fi

                PS_BG_LAST=1
                PS1+="$text"
            else
                # Draw a colored separator if we need to
                PS1+=$PS_RESET
                if [ -z "$PS_BG_LAST" ]; then
                    PS1+="$bgcolor$fgcolor"
                else
                    local next_color=$(ps_find_index $bgcolor ${PS_BG_COLORS[*]})
                    if [ "$sep_color" == "$next_color" ]; then
                        PS1+="$bgcolor$PS_FG_LAST$PS_SHORT_SEPARATOR$fgcolor"
                    else
                        PS1+="$bgcolor${PS_FG_COLORS[$sep_color]}$PS_SEPARATOR$fgcolor"
                    fi
                fi

                PS_BG_LAST=$bgcolor
                PS_FG_LAST=$fgcolor
                if [ "$bgcolor$fgcolor$text" == "\n" ]; then
                    PS1+="$PS_RESET\n"
                else
                    PS1+=" $text $PS_RESET"
                fi
            fi
        fi
    }

    ps_finish() {
        if [ "$PS_NO_COLOR" == "true" ]; then
            PS1+=" $PS_SHORT_SEPARATOR"
        else
            local sep_color=$(ps_find_index $PS_BG_LAST ${PS_BG_COLORS[*]})
            if [ -n "$sep_color" ]; then
                PS1+="${PS_FG_COLORS[$sep_color]}$PS_SEPARATOR$PS_RESET"
            fi
        fi

        unset PS_FG_LAST
        unset PS_BG_LAST
        if [ $1 -eq 1 ]; then
            PS1+=" "
        fi
    }

    # == Various displays ============================

    ps_display_date() {
        printf "$PS_BG_DARKGREY $PS_FG_WHITE $(date "+$PS_DATE_FORMAT")"
    }

    ps_display_datetime() {
        printf "$PS_BG_DARKGREY $PS_FG_WHITE $(date "+$PS_DATE_FORMAT $PS_TIME_FORMAT")"
    }

    ps_display_exitcode() {
        if [ $PS_LAST_STATUS -ne 0 ]; then
            printf "$PS_BG_RED $PS_FG_WHITE ✖ $PS_LAST_STATUS"
        fi
    }

    ps_display_gitbranch() {
        pushd . >& /dev/null
        while [ "$PWD" != "/" ] && [ ! -r "$PWD/.git/HEAD" ]; do cd ..; done

        if [ "$PWD" != "/" ]; then
            printf "$PS_BG_PURPLE $PS_FG_WHITE $(head -n1 "$PWD/.git/HEAD" | cut -d/ -f3-)"
        fi
        popd >& /dev/null
    }

    ps_display_git() {
        # Make sure git is installed
        [ -x "$(which git)" ] || return

        # get current branch name or short SHA1 hash for detached head
        local branch="$(git symbolic-ref --short HEAD 2>/dev/null || git describe --tags --always 2>/dev/null)"
        [ -n "$branch" ] || return

        local bg
        local marks
        if [ -n "$(git status --porcelain)" ]; then
            bg=$PS_BG_BRIGHTRED
            if [ "$PS_NO_COLOR" == "true" ]; then
                marks+=" +"
            fi
        else
            bg=$PS_BG_GREEN
        fi

        local stat="$(git status --porcelain --branch | grep '^##' | grep -o '\[.\+\]$')"
        local aheadN="$(echo $stat | grep -o 'ahead \d\+' | grep -o '\d\+')"
        [ -n "$aheadN" ] && marks+=" ⇡$aheadN"

        [ -n "$behindN" ] && marks+=" ⇣$behindN"
        local behindN="$(echo $stat | grep -o 'behind \d\+' | grep -o '\d\+')"
        printf "$bg $PS_FG_WHITE ⑂ $branch$marks"
    }

    ps_display_mercurial() {
        [ -x "$(which hg)" ] || return

        local branch=$(hg id -b 2>/dev/null)
        [ -n "$branch" ] || return

        local bg
        local marks
        if [ -n "$(hg status)" ]; then
            bg=$PS_BG_BRIGHTRED
            if [ "$PS_NO_COLOR" == "true" ]; then
                marks+=" +"
            fi
        else
            bg=$PS_BG_GREEN
        fi

        # Incoming/Outgoing is really slow, revisit this.
        # local marks
        # local aheadN="$(hg outgoing -q --template "{node}\n" | wc -l)"
        # [ -n "$aheadN" ] && marks+=" ⇡$aheadN"

        # local behindN="$(hg incoming -q --template "{node}\n" | wc -l)"
        # [ -n "$behindN" ] && marks+=" ⇣$behindN"

        printf "$bg $PS_FG_WHITE ⑂ $branch$marks"

    }

    ps_display_os() {
        local symbol

        case "$(uname)" in
            Darwin)
                symbol=''
                ;;
            Linux)
                symbol='$'
                ;;
            *)
                symbol='%'
        esac

        local color=$PS_FG_GREEN
        if [ $PS_LAST_STATUS -ne 0 ]; then
            color=$PS_FG_RED

            if [ "$PS_NO_COLOR" == "true" ]; then
                symbol='✖'
            fi
        fi

        printf "$PS_BG_DARKGREY $color $symbol"
    }

    ps_display_path() {
        printf "$PS_BG_BLUE $PS_FG_WHITE $PWD"
    }

    ps_display_newline() {
        if [ $1 -eq 1 ] || [ $PS_COLS -gt 0 ]; then
            printf "nl"
        fi
    }

    ps_display_root() {
        if [ "$UID" == "0" ]; then
            printf "$PS_BG_RED $PS_FG_WHITE $USER"
        fi
    }

    ps_display_time() {
        printf "$PS_BG_DARKGREY $PS_FG_WHITE $(date "+$PS_TIME_FORMAT")"
    }

    ps_display_username() {
        if [ "$UID" == "0" ]; then
            printf "$PS_BG_RED $PS_FG_WHITE $USER"
        else
            printf "$PS_BG_CYAN $PS_FG_WHITE $USER"
        fi
    }

    ps_display_virtualenv() {
        local environment="${VIRTUAL_ENV#$WORKON_HOME/}"
        [ -n "$environment" ] || return

        printf "$PS_BG_LIGHTGREY $PS_FG_WHITE $environment"
    }

    # == Execution ================

    # Declare the color lists so we can look them up later.
    ps_load_color_file
    ps_load_settings_file
    PS_BG_COLORS=($PS_BG_BLACK $PS_BG_RED $PS_BG_GREEN $PS_BG_YELLOW $PS_BG_BLUE $PS_BG_PURPLE $PS_BG_CYAN $PS_BG_LIGHTGREY $PS_BG_DARKGREY $PS_BG_BRIGHTRED $PS_BG_BRIGHTGREEN $PS_BG_BRIGHTYELLOW $PS_BG_BRIGHTBLUE $PS_BG_BRIGHTPURPLE $PS_BG_BRIGHTCYAN $PS_BG_WHITE)
    PS_FG_COLORS=($PS_FG_BLACK $PS_FG_RED $PS_FG_GREEN $PS_FG_YELLOW $PS_FG_BLUE $PS_FG_PURPLE $PS_FG_CYAN $PS_FG_LIGHTGREY $PS_FG_DARKGREY $PS_FG_BRIGHTRED $PS_FG_BRIGHTGREEN $PS_FG_BRIGHTYELLOW $PS_FG_BRIGHTBLUE $PS_FG_BRIGHTPURPLE $PS_FG_BRIGHTCYAN $PS_FG_WHITE)

    # State
    PS_BG_LAST=""
    PS_FG_LAST=""
    PS_LAST_STATUS=0
    PS_NO_COLOR=""
    PS_SEPARATOR=''
    PS_SHORT_SEPARATOR=''

    ps1() {
        PS_LAST_STATUS=$?
        PS_COL_WIDTH=$(($(tput cols) - 10 ))
        if [ $PS_COL_WIDTH -lt 1 ]; then
            PS_COL_WIDTH=1
        fi

        PS_COLS=0
        PS1=""
        for i in $PS_FEATURES; do
            case "$i" in
                date) ps_add $(ps_display_date) ;;
                datetime) ps_add $(ps_display_datetime) ;;
                exitcode) ps_add $(ps_display_exitcode) ;;
                forcenewline) ps_add $(ps_display_newline 1) ;;
                git) ps_add $(ps_display_git) ;;
                gitbranch) ps_add $(ps_display_gitbranch) ;;
                mercurial) ps_add $(ps_display_mercurial) ;;
                newline) ps_add $(ps_display_newline 0) ;;
                os) ps_add $(ps_display_os) ;;
                path) ps_add $(ps_display_path) ;;
                root) ps_add $(ps_display_root) ;;
                time) ps_add $(ps_display_time) ;;
                username) ps_add $(ps_display_username) ;;
                virtualenv) ps_add $(ps_display_virtualenv) ;;
            esac
        done

        if [ -n "$PS_BG_LAST$PS_FG_LAST" ]; then
            ps_finish 1
        fi
    }

    PROMPT_COMMAND=ps1
}

__ps_init
unset __ps_init
